import csv
from itertools import izip, chain
from operator import itemgetter

wins_player = {}
# Rounds for each Tournament
def round(file_name, round_number, sex):

    round_players = {'1':32,'2':16,'3':8,'4':4,'5':2}

    if file_name == 'quit':
        exit(0)
    elif file_name == 'input':
        with open('{}{}_input.csv'.format(round_number,sex),'w') as f:
                  wr = csv.writer(f)
                  wr.writerow(["Player A,Player A Score, Player B, Player B Score"])
                  for x in range(0,round_players[str(round_number)]/2): # loop depending on the round
                      player_a_name = raw_input("Enter Player A Name ")
                      player_a_score = int(input("Enter Player A Score "))
                      player_b_name = raw_input("Enter Player BName ")
                      player_b_score = int(input("Enter Player B Score "))
                      wr.writerow([player_a_name,player_a_score,player_b_name,player_b_score])
        file_name ="{}{}_input".format(round_number,sex)

    # Assigning the filename an extension
    f_name = '{}.csv'.format(file_name)

    # Check round number
    if round_number == 1:
        range_x = 16
        points = 5

    elif round_number ==2 :
        range_x = 8
        points = 10

    elif round_number == 3:
        range_x = 4
        points = 30

    elif round_number == 4:
        range_x = 2
        points = 50

    elif round_number == 5:
        range_x = 1
        points = 100

    else:
        print ("Round number is out of range")

    players_qualified = []
    tr_points = []
    r_points = []


    # Assign ranking points to each winner
    for e in range(range_x):
        r_points.append(points)

    with open(f_name, 'r') as data_file:
        csv_data = csv.reader(data_file)
        # We dont want headers
        next(csv_data)
        # Get list of players who won
        for line in csv_data:
            if line[1] > line[3]:
                players_qualified.append(line[0])
                point_diff = int(line[1]) - int(line[3])
                tr_points.append(point_diff)
            elif line[3] > line[1]:
                players_qualified.append(line[2])
                point_diff = int(line[3]) - int(line[1])
                tr_points.append(point_diff)

    # print players_qualified
    # Write into file per round so as to persist the data
    with open("{}_POINTS_{}.csv".format(round_number, sex), 'w') as rpm:
        player_id = []
        for e in players_qualified:
            player_id.append(e)
        rows = zip(player_id, r_points )
        writer = csv.writer(rpm)
        for row in rows:
            writer.writerow(row)

        for each in rows:
            print str(each).replace('(','').replace(')','').replace("'", '')

# Function that removes duplicate values in a dictionary
def remove_dups(data_one, data_two):
    new = []
    for x in data_one:
        for y in data_two:
            if x[0] == y[0]:
                new.append(x[0])
    for x in data_one:
        if x[0] in new:
            data_one.remove(x)

    return data_one

# Function that handles writing into files
def write_file(list_a, list_b, list_c, sex,filename,mode):
    with open(filename,mode) as xf:
        csv_writer = csv.writer(xf)
        csv_writer.writerows(izip(list_a, list_b, list_c,
                                                [i for i in range(1,len(list_a)+1)],
                                                [current_tournament for i in range(0,len(list_a))],
                                                [wins_player[x] for x in list_a]))

# Function for the user input in the console & wrtting to file
def user_input(filename,sex):
    f_name = '{}.csv'.format(filename)
    # Open file to insert the user input
    f = open('userinput.csv', 'w')
    # Create headers for the file
    header = {'Player A Score', 'Player B Score'}
    with f:
        wr = csv.DictWriter(f, fieldnames=header)
        wr.writeheader()
        #  read round file to iterate the number of times in that round
        with open(f_name, 'rb') as d:
            rd = csv.reader(d)
            for x in rd:
                scores_a = int(input("Enter Player A Score "))
                # Either the player A is injured or user can re enter the score for the player A
                if scores_a >= 4:
                    c1 = raw_input("Invalid Score is PLAYER A injured? 1 = YES , 2 = RE ENTER SCORE ")
                    if c1 == "1":
                        scores_a = 0
                        wr.writerow({'Player A Score': scores_a})
                        print "PLAYER A looses"
                    elif c1 == "2":
                        scores_a = int(input("RE ENTER PLAYER A score "))
                        wr.writerow({'Player A Score': scores_a})
                    else:
                        print "error"

                scores_b = int(input("Enter Player B Score "))
                # Either the player B is injured or user can re enter the score for the player B
                if scores_b >= 4:
                    c1 = raw_input("Invalid Score is PLAYER B injured? 1 = YES , 2 = RE ENTER SCORE ")
                    if c1 == "1":
                        scores_b = 0
                        wr.writerow({'Player B Score': scores_b})
                        print "PLAYER B looses"
                    elif c1 == "2":
                        scores_b = int(input("RE ENTER PLAYER B score "))
                        wr.writerow({'Player B Score': scores_b})
                    else:
                        print "error"

                elif sex == 1 & scores_a - scores_b == 1:
                    print "Not a valid score, Male Player has to win by 2"

                elif sex == 2 & scores_a & scores_b ==0:
                    print"Not a valid score, Female Player has to wind by 1"

                else:
                    wr.writerow({'Player A Score': scores_a, 'Player B Score': scores_b})

#writes the user input and scores to file
def write_file_user(filename,sex):
    f_name = '{}.csv'.format(filename)

    # Open round file and append the players from that round
    with open(f_name, 'rb') as csvfile:
        reader = csv.reader(csvfile)
        player_a = []
        player_b = []
        for row in reader:
            player_a.append(row[0])
            player_b.append(row[2])

    user_input(filename,sex)

    # Append user scores from the userinput file
    with open('userinput.csv','rb') as filecsv:
        reader = csv.reader(filecsv)
        player_a_score = []
        player_b_score = []
        for x in reader:
            player_a_score.append(x[1])
            player_b_score.append(x[0])

    # Open a file to then zip the players and scores in same file
    # (this file is then used to process the scores)
    with open('merged.csv','w') as file:
        rows = zip(player_a,player_a_score,player_b,player_b_score)
        writer = csv.writer(file)
        for row in rows:
            writer.writerow(row)

# Function to read files
def read_file(filename):
    with open(filename, 'r') as rf:
        reader = rf.read()
        print reader

# Function that returns results of each tournament
def tournament(difficulty):

    # Let the user select the gender
    gender = raw_input("Enter 1 For Men and 2 for ladies: ")
    if gender == "1":
        sex = "MEN"
    elif gender == "2":
        sex = "LADIES"
    else:
        print ("That is not a valid category")

    print "If you want to enter scores manuallythen enter 'input' If you wan to quit enter 'quit' "
    console_display = "If not kindly enter the file name for the  $1 Round. "
    round_1 = raw_input(console_display.replace('$1','First'))
    round(round_1,1,sex)
    round_2 = raw_input(console_display.replace('$1','Second'))
    round(round_2,2,sex)
    round_3 = raw_input(console_display.replace('$1','Third'))
    round(round_3,3,sex)
    round_4 = raw_input(console_display.replace('$1','Fourth'))
    round(round_4,4,sex)
    round_5 = raw_input(console_display.replace('$1','Final'))
    round(round_5,5,sex)

    # Open files to accumulate and create on list of all players and total points in the tournament
    data_one = []
    data_two = []
    data_three = []
    data_four = []
    data_five =[]

    # Select the gender for the tournament
    if gender == "1":
        sex = "MEN"
    elif gender == "2":
        sex = "LADIES"
    else:
        print ("That is not a valid category")



    # Read round results
    read_point = open("{}_POINTS_{}.csv".format(1, sex), 'r')
    data = read_point.read()
    data_list_1 = data.split('\r\n')
    for e in data_list_1:
        try:
            a = score[0]
        except:
            a = ' '
        score = e.split(',')
        if a != score[0]:
            wins_player[score[0]] = int(score[2])
            data_one.append(list(score))
    read_point.close()
    score = None

    # Read round results
    read_point = open("{}_POINTS_{}.csv".format(3, sex), 'r')
    data = read_point.read()
    data = data.strip()
    data_list_3 = data.split('\r\n')
    for e in data_list_3:
        try:
            a = score[0]
        except:
            a = ' '
        score = e.split(' ,')
        if a != score[0]:
            wins_player[score[0]] + int(score[2])
            data_three.append(list(score))
    read_point.close()
    score = None

    # Read round results
    read_point = open("{}_POINTS_{}.csv".format(4, sex), 'r')
    data = read_point.read()
    data = data.strip()
    data_list_4 = data.split('\r\n')
    for e in data_list_4:
        try:
            a = score[0]
        except:
            a = ' '
        score = e.split(' ,')
        if a != score[0]:
            wins_player[score[0]] + int(score[2])
            data_four.append(list(score))
    read_point.close()
    score = None


    # Read round results
    read_point = open("{}_POINTS_{}.csv".format(5, sex), 'r')
    data = read_point.read()
    data = data.strip()
    data_list_5 = data.split('\r\n')
    for e in data_list_5:
        try:
            a = score[0]
        except:
            a = ' '
        score = e.split(' ,')
        if a != score[0]:
            wins_player[score[0]] + int(score[2])
            data_five.append(list(score))
    read_point.close()
    score = None

    # Remove all duplicate entries
    unique_1 = remove_dups(data_one, data_two)
    unique_2 = remove_dups(data_two, data_three)
    unique_3 = remove_dups(data_three, data_four)
    unique_4 = remove_dups(data_four, data_five)

    # Combine list of all winners in all rounds
    merged_list = (data_five+unique_4+unique_3+unique_2+unique_1)

    for e in merged_list:
        e[1] = int(e[1]) * difficulty

    # Get prize
    with open("PRIZE_MONEY.csv", 'r') as pm:
        csv_reader = csv.reader(pm)
        prize_list = []
        next(csv_reader)
        for e in csv_reader:
            prize_list.append(e[2])

    # Calculate prize reward in regards to difficulty of tournament
    if difficulty == 2.7:
        money = prize_list[:8:]
    elif difficulty == 2.3:
        money = prize_list[8:16:]
    elif difficulty == 3.1:
        money = prize_list[16:24:]
    elif difficulty == 3.25:
        money = prize_list[24:32:]

    for i in range(8):
        money.append(0)

    players = []
    tournament_points = []
    for x in merged_list:
        players.append(x[0])
        tournament_points.append(x[1])

    # Remove duplicate players from players list
    new_list = []
    for each in players:
        if each not in new_list:
            new_list.append(each)

    # Write in the tournament results to file
    write_file(new_list,tournament_points, money, sex, "TOURNAMENT_RESULTS_{}.csv".format(sex),'w')
    write_file(new_list,tournament_points, money, sex, "SEASON_RESULTS_{}.csv".format(sex),'a')

    print """
        1. Tournament ranking
        2. Season Ranking
        3. Next Tournament
        4. Start new Season
        5. Exit/QUIT

        """
    choice = raw_input('Choose Option: ')
    if choice == '1':
        print "Player ranking, Tournament Points and money accumulayed Succesfully written"
        read_file("TOURNAMENT_RESULTS_{}.csv".format(sex))
    elif choice == '2':
        get_season_rank(2)
    elif choice == '3':
        pass
    elif choice == '4':
        try:
            remove('SEASON_RESULTS_LADIES.csv')
        except:
            pass
        try:
            remove('SEASON_RESULTS_MEN.csv')
        except:
            pass
        else:
            print "Goodbye"
            exit(0)

    # Print out tournament results


def get_season_rank(sex):
    with open("SEASON_RESULTS_{}.csv".format(sex),'rb') as csvfile:
        reader = csv.reader(csvfile)
        stats = list(reader)
        stats = sorted(stats, key=lambda k: k[0]) # sort the stats, ordering the rows by player name, so same player entries for different tournament are together
        stat_list #empty list that will store the players name, rank and score
        #yields the elemtns of the iterator as well as an index round_number:
        for ind, stat in enumerate(stats):
            names = [x[0] for x in stat_list] #get the player names from stats_list for duplication Check
            #if stat_list does not contain the playe rname we append it
            if stat[0] not in names or ind == 0:
                stat_list.append([stat[0],stat[1]]) #append the name and score of player
            else:
                dup_index =[i for i, x in enumerate(stat_list) if x[0] == stat[0]][0] #if stat_list has players data we modify the current entry
                stat_list[dup_index] = [stat[0], float(stat_list[dup_index][1]) + float(stat[1])]   # we display the sum of score for all touraments in season
        print 'SEASON STATS'
        print 'Rank, Player, Score'
        #sort the list of players so players with high scores come first
        stat_list = sorted(stat_list, key=itemgetter(1), reverse = True)
        for ind, stat in enumerate(stat_list):
            print ind + 1, '       ', stat[0], '      ', stat[1]








# Create menu options
ans=True
while ans:
    print """
    1. TAC
    2. TAE21
    3. TAW11
    4. TBS2
    5. Exit/QUIT

    """
    ans = raw_input("Choose Tournament: ")

    if ans == '1':
        print "\n Entering TAC"
        tournament(2.7)
    elif ans == '2':
        print "\n Entering TAE21"
        tournament(2.3)
    elif ans == '3':
        print "\n Entering TAW11"
        tournament(3.1)
    elif ans == '4':
        print "\n Entering TBS2"
        tournament(3.25)
    elif ans == '5':
        print "\n Goodbye"
        ans = False
    elif ans != "":
        print "\n Enter a valid choice"
