import csv
from itertools import izip, chain
from os import remove
from operator import itemgetter

wins_player = {}


# Function that gets season rank
def get_season_rank(sex):
    with open("SEASON_RESULTS_{}.csv".format(sex), 'rb') as csvFile:
        reader = csv.reader(csvFile)
        stats = list(reader)
        # stats = stats.sort()
        stats = sorted(stats, key=lambda k: k[0])
        stat_list = []
        for ind, stat in enumerate(stats):
            names = [x[0] for x in stat_list]
            if stat[0] not in names or ind == 0:
                stat_list.append([stat[0], stat[1]])
            else:
                dup_index = [i for i, x in enumerate(stat_list) if x[0] == stat[0]][0]
                stat_list[dup_index] = [stat[0], float(stat_list[dup_index][1]) + float(stat[1])]
        print 'Season Stats'
        print 'Rank, Player, Score'
        stat_list = sorted(stat_list, key=itemgetter(1), reverse=True)
        for ind, stat in enumerate(stat_list):
            print ind + 1, '   ', stat[0], '   ', stat[1]

# Rounds for each Tournament
def round(file_name, round_number, sex):
    # dictionary containing round and number of participants in each round
    partic_round = {'1':32, '2':16, '3':8, '4':4, '5':2}

    # Assigning the filename an extension
    if file_name == 'quit':
        exit(0)

    # creates a file for manually entered scores
    elif file_name == 'custom':
        with open('{}{}_custom.csv'.format(round_number,sex),'w') as csvFile:
            writer = csv.writer(csvFile)
            writer.writerow(["Player A","Score Player A","Player B","Score Player B"])
            for i in range(0,partic_round[str(round_number)]/2):
                player1_name = raw_input('Enter player A name')
                player1_score = raw_input('Enter player A score')
                player2_name = raw_input('Enter player B name')
                player2_score = raw_input('Enter player B score')
                writer.writerow([player1_name,player1_score,player2_name,player2_score])
        file_name = "{}{}_custom".format(round_number,sex)


    f_name = '{}.csv'.format(file_name)

    # Check round number
    if round_number == 1:
        range_x = 16
        points = 5

    elif round_number ==2 :
        range_x = 8
        points = 10

    elif round_number == 3:
        range_x = 4
        points = 30

    elif round_number == 4:
        range_x = 2
        points = 50

    elif round_number == 5:
        range_x = 1
        points = 100

    else:
        print ("Round number is out of range")

    players_qualified = []
    tr_points = []
    r_points = []
    # Assign ranking points to each winner
    for e in range(range_x):
        r_points.append(points)


    with open(f_name, 'r') as data_file:
        csv_data = csv.reader(data_file)
        # We dont want headers
        next(csv_data)
        # Get list of players who won
        for line in csv_data:
            if line[1] > line[3]:
                players_qualified.append(line[0])
                point_diff = int(line[1]) - int(line[3])
                tr_points.append(point_diff)
            elif line[3] > line[1]:
                players_qualified.append(line[2])
                point_diff = int(line[3]) - int(line[1])
                tr_points.append(point_diff)

    # print players_qualified
    # Write into file per round so as to persist the data
    with open("{}_POINTS_{}.csv".format(round_number, sex), 'w') as rpm:
        player_id = []
        for e in players_qualified:
            player_id.append(e)
        rows = zip(player_id, r_points,[1 for i in range(0,len(player_id))])
        writer = csv.writer(rpm)
        for row in rows:
            writer.writerow(row)

        for each in rows:
            print str(each).replace('(','').replace(')','').replace("'", '')

# Function that removes duplicate values in a dictionary
def remove_dups(data_one, data_two):
    new = []
    for x in data_one:
        for y in data_two:
            if x[0] == y[0]:
                new.append(x[0])
    for x in data_one:
        if x[0] in new:
            data_one.remove(x)

    return data_one

# Function that handles writing into files
def write_file(list_a, list_b, list_c, sex,filename,mode):
    with open(filename, mode) as xf:
        csv_writer = csv.writer(xf)
        # Writing ranking as well as tournament name as well in the csv file
        csv_writer.writerows(izip(list_a, list_b, list_c,
                                  [i for i in range(1,len(list_a)+1)],
                                  [current_tournament for i in range(0,len(list_a))],
                                  [wins_player[x] for x in list_a]))

# Function to read files
def read_file(filename):
    with open(filename, 'r') as rf:
        reader = rf.read()
        print reader

# Function that returns results of each tournament
def tournament(difficulty):

    # Let the user select the gender
    gender = raw_input("Enter 1 For Men and 2 for Ladies: ")
    if gender == "1":
        sex = "MEN"
    elif gender == "2":
        sex = "LADIES"
    else:
        print ("That is not a valid category")

    # Key in results for each round and simulate winners
    str_disp = "Kindly enter the file name for the result of the $1 round. If you want to enter scores personally then enter 'custom' or if you want to quit enter 'quit' : "
    round_1 = raw_input(str_disp.replace('$1','First'))
    round(round_1, 1, sex)
    round_2 = raw_input(str_disp.replace('$1','Second'))
    round(round_2, 2, sex)
    round_3 = raw_input(str_disp.replace('$1','Third'))
    round(round_3, 3, sex)
    round_4 = raw_input(str_disp.replace('$1','Fourth'))
    round(round_4, 4, sex)
    round_f = raw_input(str_disp.replace('$1','Final'))
    round(round_f, 5, sex)

    # Open files to accumulate and create on list of all players and total points in the tournament
    data_one = []
    data_two = []
    data_three = []
    data_four = []
    data_five =[]

    # Select the gender for the tournament
    if gender == "1":
        sex = "MEN"
    elif gender == "2":
        sex = "LADIES"
    else:
        print ("That is not a valid category")


    # Read round results
    read_point = open("{}_POINTS_{}.csv".format(1, sex), 'r')
    data = read_point.read()
    data = data.strip()
    data_list_1 = data.split('\r\n')
    for e in data_list_1:
        try:
            a = score[0]
        except:
            a = ''
        score = e.split(',')
        if a != score[0]:
            wins_player[score[0]] = int(score[2])
            data_one.append(list(score))
    read_point.close()
    score = None

    # Read round results
    read_point = open("{}_POINTS_{}.csv".format(2,sex), 'r')
    data = read_point.read()
    data = data.strip()
    data_list_2 = data.split('\r\n')
    for e in data_list_2:
        try:
            a = score[0]
        except:
            a = ''
        score = e.split(',')
        if a != score[0]:
            wins_player[score[0]] = wins_player[score[0]] + int(score[2])
            data_two.append(list(score))
    read_point.close()
    score = None

    # Read round results
    read_point = open("{}_POINTS_{}.csv".format(3, sex), 'r')
    data = read_point.read()
    data = data.strip()
    data_list_3 = data.split('\r\n')
    for e in data_list_3:
        try:
            a = score[0]
        except:
            a = ''
        score = e.split(',')
        if a != score[0]:
            wins_player[score[0]] = wins_player[score[0]] + int(score[2])
            data_three.append(list(score))
    read_point.close()
    score = None

    # Read round results
    read_point = open("{}_POINTS_{}.csv".format(4, sex), 'r')
    data = read_point.read()
    data = data.strip()
    data_list_4 = data.split('\r\n')
    for e in data_list_4:
        try:
            a = score[0]
        except:
            a = ''
        score = e.split(',')
        if a != score[0]:
            wins_player[score[0]] = wins_player[score[0]] + int(score[2])
            data_four.append(list(score))
    read_point.close()
    score = None

    # Read round results
    read_point = open("{}_POINTS_{}.csv".format(5, sex), 'r')
    data = read_point.read()
    data = data.strip()
    data_list_5 = data.split('\r\n')
    for e in data_list_5:
        try:
            a = score[0]
        except:
            a = ''
        score = e.split(',')
        if a != score[0]:
            wins_player[score[0]] = wins_player[score[0]] + int(score[2])
            data_five.append(list(score))
    read_point.close()
    score = None


    # Remove all duplicate entries
    unique_1 = remove_dups(data_one, data_two)
    unique_2 = remove_dups(data_two, data_three)
    unique_3 = remove_dups(data_three, data_four)
    unique_4 = remove_dups(data_four, data_five)

    # Combine list of all winners in all rounds
    merged_list = (data_five+unique_4+unique_3+unique_2+unique_1)

    for e in merged_list:
        e[1] = int(e[1]) * difficulty

    # Get prize
    with open("PRIZE_MONEY.csv", 'r') as pm:
        csv_reader = csv.reader(pm)
        prize_list = []
        next(csv_reader)
        for e in csv_reader:
            prize_list.append(e[2])

    # Calculate prize reward in regards to difficulty of tournament
    if difficulty == 2.7:
        money = prize_list[:8:]
    elif difficulty == 2.3:
        money = prize_list[8:16:]
    elif difficulty == 3.1:
        money = prize_list[16:24:]
    elif difficulty == 3.25:
        money = prize_list[24:32:]

    for i in range(8):
        money.append(0)

    players = []
    tournament_points = []
    for x in merged_list:
        players.append(x[0])
        tournament_points.append(x[1])

    # Remove duplicate players from players list
    new_list = []
    for each in players:
        if each not in new_list:
            new_list.append(each)

    # Write in the tournament results to file
    write_file(new_list,tournament_points,  money, sex,"TOURNAMENT_RESULTS_{}.csv".format(sex),'w')
    # Write tournament results to the season file
    write_file(new_list,tournament_points,  money, sex,"SEASON_RESULTS_{}.csv".format(sex),'a')


    # Options to either continue with the program or exit or watch stats
    print """
        1. Tournament Ranking
        2. Season Ranking
        3. Next Tournament
        4. Exit
        """
    bla = raw_input('Choose Option : ')
    if bla == '1':
        # Print out tournament results
        print "Player ranking, Tournament Points and money accumulayed Succesfully written"
        read_file("TOURNAMENT_RESULTS_{}.csv".format(sex))
    elif bla == '2':
        get_season_rank(sex)
    elif bla == '3':
        pass
    else:
        try:
            # Deletes the season files
            remove('SEASON_RESULTS_LADIES.csv')
        except:
            pass
        try:
            remove('SEASON_RESULTS_MEN.csv')
        except:
            pass
        exit(0)


# Create menu options
ans=True
while ans:
    print """
    1. TAC
    2. TAE21
    3. TAW11
    4. TBS2
    5. Exit/QUIT

    """
    ans = raw_input("Choose Tournament: ")

    if ans == '1':
        current_tournament = "TAC"
        print "\n Entering TAC"
        tournament(2.7)
    elif ans == '2':
        current_tournament = "TAE21"
        print "\n Entering TAE21"
        tournament(2.3)
    elif ans == '3':
        current_tournament = "TAW11"
        print "\n Entering TAW11"
        tournament(3.1)
    elif ans == '4':
        current_tournament = "TBS2"
        print "\n Entering TBS2"
        tournament(3.25)
    elif ans == '5':
        print "\n Goodbye"
        ans = False
        try:
            # Deletes the season files
            remove('SEASON_RESULTS_LADIES.csv')
        except:
            pass
        try:
            remove('SEASON_RESULTS_MEN.csv')
        except:
            pass
    elif ans != "":
        print "\n Enter a valid choice"
